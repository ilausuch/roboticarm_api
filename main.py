from __future__ import division
from flask import Flask, request
from flask_restful import Resource, Api, reqparse
from json import dumps
from flask_jsonpify import jsonify
import time
import pickle
from flask_cors import CORS

useController = True

try:
    import Adafruit_PCA9685
except ImportError:
    useController = False

if useController:
    pwm = Adafruit_PCA9685.PCA9685()
    pwm.set_pwm_freq(50)
else:
    print("No controller")

app = Flask(__name__)
CORS(app)
api = Api(app)

servo_range_min=[150, 150, 150, 150, 150, 150]
servo_range_max=[600, 600, 600, 600, 600, 600]
servos = [0.5, 0.5, 0.5, 0.5, 0.5, 0.5]
endPoints = dict()

def set_servo(id, value):
    global useController
    global servos

    servos[id] = float(value)
    if useController:
        value = int(servos[id] * (servo_range_max[id] - servo_range_min[id]) + servo_range_min[id])
        pwm.set_pwm(id, 0, value)
        print("Servo %d set to %d" % (id, value))
    else:
        value = int(servos[id] * (servo_range_max[id] - servo_range_min[id]) + servo_range_min[id])
        print("Simulated Servo %d set to %d" % (id, value))

def save_obj(obj, name ):
    with open('data/'+ name + '.pkl', 'wb') as f:
        pickle.dump(obj, f)

def load_obj(name ):
    with open('data/' + name + '.pkl', 'rb') as f:
        return pickle.load(f)

class ApiLimits(Resource):
    def get(self):
        global servo_range_min
        global servo_range_max

        return {'min': servo_range_min, "max":servo_range_max}

class ApiLimit(Resource):
    def post(self, id):
        global servo_range_min
        global servo_range_max

        parser = reqparse.RequestParser()
        parser.add_argument('action')
        parser.add_argument('min')
        parser.add_argument('max')

        args = parser.parse_args()
        min = int(args.min)
        max = int(args.max)
        id = int(id)

        if args.action == "set":
            servo_range_max[id] = max
            save_obj(servo_range_max, "servo_range_max")
            servo_range_min[id] = min
            save_obj(servo_range_min, "servo_range_min")

        return {'status':'ok', 'args':args}


class ApiServos(Resource):
    def get(self):
        global servos
        return {'Servos': servos} # Fetches first column that is Employee ID

class ApiServo(Resource):
    def post(self, id):
        global servos
        global useController

        parser = reqparse.RequestParser()
        parser.add_argument('value')
        args = parser.parse_args()

        id = int(id)
        set_servo(id, float(args.value))

        return {'status':'ok', 'args':args}

class ApiEndPoints(Resource):
    def get(self):
        global endPoints
        return {'EndPoints': endPoints} # Fetches first column that is Employee ID

class ApiEndPoint(Resource):
    def post(self, id):
        global endPoints

        id = int(id)
        data = request.get_json()

        if data["action"] == "set":
            endPoints[id] = data["pos"]
            save_obj(endPoints, "endPoints")
            print(endPoints)
            return {'status':'ok'}

        if data["action"] == "execute":
            try:
                endPoint = endPoints[id]
            except Exception:
                return {'status':'error', "detail": "Endpoint doesn't exist"}, 400

            for i in range(len(endPoint)):
                if endPoint[i] >= 0:
                    set_servo(i, float(endPoint[i]))

            return {'status':'ok', "endPoint": endPoint}


api.add_resource(ApiLimits, '/limits')
api.add_resource(ApiLimit, '/limits/<id>')

api.add_resource(ApiServos, '/servo')
api.add_resource(ApiServo, '/servo/<id>')

api.add_resource(ApiEndPoints, '/endPoint')
api.add_resource(ApiEndPoint, '/endPoint/<id>')

try:
    endPoints = load_obj("endPoints")
except Exception:
    pass

try:
    servo_range_max = load_obj("servo_range_max")
except Exception:
    pass

try:
    servo_range_min = load_obj("servo_range_min")
except Exception:
    pass

if __name__ == '__main__':
     app.run(host= '0.0.0.0', port='5002')
